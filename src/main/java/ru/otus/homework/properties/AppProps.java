package ru.otus.homework.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Настройки и (вычислимые) свойства приложения
 */
@Component
public class AppProps {

    /**
     * Текущий язык
     */
    private final String currentLanguage;

    /**
     * Текущая страна
     */
    private final String currentCountry;

    /**
     * Текущая локаль
     */
    private final Locale currentLocale;

    /**
     * Путь к файлу с вопросами теста и ответами без учёта локализации
     */
    private String testCsvPath;

    /**
     * Путь к локализованному файлу с вопросами теста и ответами
     */
    private final String localizedCsvFilePath;


    // Constructors ---

    /**
     * @param currentLanguage текущий язык
     * @param currentCountry текущая страна
     * @param testCsvPath путь к файлу с вопросами теста и ответами без учёта локализации
     */
    public AppProps(
            @Value("${language:en}") final String currentLanguage,
            @Value("${country:EN}") final String currentCountry,
            @Value("${test.csv.path}") final String testCsvPath) {

        this.currentLanguage = currentLanguage;
        this.currentCountry = currentCountry;
        this.testCsvPath = testCsvPath;

        this.currentLocale = new Locale(this.currentLanguage.toLowerCase(), this.currentCountry.toUpperCase());
        this.localizedCsvFilePath = this.testCsvPath + "_" + currentLanguage.toLowerCase() + "_" + currentCountry.toUpperCase() + ".csv";
    }

    // Getters ---

    public Locale getCurrentLocale() {
        return currentLocale;
    }

    public String getLocalizedCsvFilePath() {
        return localizedCsvFilePath;
    }
}

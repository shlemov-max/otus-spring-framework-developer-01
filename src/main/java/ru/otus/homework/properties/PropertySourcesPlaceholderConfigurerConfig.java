package ru.otus.homework.properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Конфигурация для чтения настроек из файла application.properties.
 */
@Configuration
public class PropertySourcesPlaceholderConfigurerConfig {

    /**
     * Настройка PlaceholderConfigurerSupport.
     *
     * После добавления этого Bean, параметр "language" стал нормально считываться.
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}

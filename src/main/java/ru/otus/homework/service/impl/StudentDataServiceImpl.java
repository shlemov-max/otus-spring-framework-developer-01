package ru.otus.homework.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Student;
import ru.otus.homework.localization.MessageSourceService;
import ru.otus.homework.service.StudentDataService;

import java.util.Scanner;

/**
 * Сервис сбора данных о студенте.
 */
@Service("studentDataService")
public class StudentDataServiceImpl implements StudentDataService {

    /**
     * Источник локализованных сообщений.
     */
    private final MessageSourceService messageSourceService;

    // Constructors ---
    /**
     * @param messageSourceService обёртка источника локализованных сообщений.
     */
    @Autowired
    public StudentDataServiceImpl(final MessageSourceService messageSourceService) {
        this.messageSourceService = messageSourceService;
    }

    // Public methods ---

    /**
     * Получить данные о студенте.
     *
     * @return данные о студенте
     */
    @Override
    public Student getStudent() {

        final Scanner in = new Scanner(System.in);

        //System.out.print("Укажите своё имя и нажмите \"Enter\": ");
        final String enterYourName = this.messageSourceService.getLocalizedMessage("enter.your.name", new String[]{});
        System.out.print(enterYourName);

        final String name = in.next();

        //System.out.print("Укажите свою фамилию и нажмите \"Enter\": ");
        final String enterYourSurname = this.messageSourceService.getLocalizedMessage("enter.your.surname", new String[]{});
        System.out.print(enterYourSurname);

        final String surname = in.next();

        //System.out.printf("Здравствуйте, %s %s!  \n", name, surname);
        final String hello = this.messageSourceService.getLocalizedMessage("hello", new String[]{name, surname});
        System.out.print(hello);

        //System.out.println("Сейчас Вам будет предложен тест. После каждого вопроса следует ввести номер правильного варианта и нажать \"Enter\".\n");
        final String nowYouWillBeOfferedTest = this.messageSourceService.getLocalizedMessage("now.you.will.be.offered.test", new String[]{});
        System.out.print(nowYouWillBeOfferedTest);

        System.out.println("");

        final String begin = in.nextLine();

        final Student student = new Student(surname, name);
        return student;
    }
}

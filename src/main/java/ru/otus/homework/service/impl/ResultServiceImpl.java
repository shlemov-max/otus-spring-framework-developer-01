package ru.otus.homework.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Student;
import ru.otus.homework.localization.MessageSourceService;
import ru.otus.homework.service.ResultService;

/**
 * Сервис вывода результатов тестирования.
 */
@Service("resultService")
public class ResultServiceImpl implements ResultService {

    /**
     * Источник локализованных сообщений.
     */
    private final MessageSourceService messageSourceService;

    // Constructors ---

    /**
     * @param messageSourceService обёртка источника локализованных сообщений
     */
    @Autowired
    public ResultServiceImpl(final MessageSourceService messageSourceService) {
        this.messageSourceService = messageSourceService;
    }

    // Public methods ---

    /**
     * Вывести результаты.
     *
     * @param student данные о студенте
     * @param mark    оценка
     */
    @Override
    public void printResults(final Student student, final int mark) {

        //System.out.println(String.format("%s %s, Ваша оценка: %d", student.getName(), student.getSurname(), mark));

        final String message = this.messageSourceService.getLocalizedMessage(
                "your.mark.is",
                new String[]{
                student.getName(),
                student.getSurname(),
                Integer.valueOf(mark).toString()});

        System.out.println(message);
    }
}

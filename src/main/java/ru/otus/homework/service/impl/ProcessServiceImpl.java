package ru.otus.homework.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Student;
import ru.otus.homework.service.ProcessService;
import ru.otus.homework.service.ResultService;
import ru.otus.homework.service.StudentDataService;
import ru.otus.homework.service.TestService;

/**
 * Основной процесс.
 */
@Service ("processService")
public class ProcessServiceImpl implements ProcessService {

    private final StudentDataService studentDataService;

    private final TestService testService;

    private final ResultService resultService;

    // Constructors ---

    /**
     * @param studentDataService сервис сбора данных о студенте
     * @param testService серсис тестирования
     * @param resultService сревис вывода результатов
     */
    @Autowired
    public ProcessServiceImpl(
            StudentDataService studentDataService,
            TestService testService,
            ResultService resultService) {

        this.studentDataService = studentDataService;
        this.testService = testService;
        this.resultService = resultService;
    }

    // Public methods --

    @Override
    public void start() {

        final Student student = this.studentDataService.getStudent();
        final int mark = this.testService.getMark();
        this.resultService.printResults(student, mark);
    }
}

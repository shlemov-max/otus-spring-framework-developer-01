package ru.otus.homework.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework.dao.QuestionDao;
import ru.otus.homework.domain.Question;
import ru.otus.homework.service.TestService;

import java.util.List;
import java.util.Scanner;

/**
 * Сервис тестирования.
 */
@Service("testService")
public class TestServiceImpl implements TestService {

    /**
     * DAO вопросов тестирования.
     */
    private final QuestionDao questionDao;


    // Constructors ---
    @Autowired
    public TestServiceImpl(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }

    // Public methods ---

    /**
     * Провести тестирование и получить оценку.
     *
     * @return оценка
     */
    @Override
    public int getMark() {

        final Scanner in = new Scanner(System.in);
        final List<Question> questionList = questionDao.getQuestionList();

        int questionCounter = 0;
        int mark = 0;
        for (Question question : questionList) {
            questionCounter++;

            System.out.println("\n" + question.toString());

            int answerVariantNumber = in.nextInt();
            if (answerVariantNumber == question.getRightVariantNumber()) {
                // Правильный ответ
                mark++;
            }
        }
        return mark;
    }
}

package ru.otus.homework.service;

import ru.otus.homework.domain.Student;

/**
 * Сервис сбора данных о студенте.
 */
public interface StudentDataService {

    /**
     * Получить данные о студенте.
     *
     * @return данные о студенте
     */
    Student getStudent();

}

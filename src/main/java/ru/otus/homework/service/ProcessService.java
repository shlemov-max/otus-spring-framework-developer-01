package ru.otus.homework.service;

/**
 * Основной процесс.
 */
public interface ProcessService {

    /**
     * Запуск процесса.
     */
    void start();

}

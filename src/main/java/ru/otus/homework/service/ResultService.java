package ru.otus.homework.service;

import ru.otus.homework.domain.Student;

/**
 * Сервис вывода результатов тестирования.
 */
public interface ResultService {

    /**
     * Вывести результаты.
     *
     * @param student данные о студенте
     * @param mark    оценка
     */
    void printResults(Student student, int mark);

}

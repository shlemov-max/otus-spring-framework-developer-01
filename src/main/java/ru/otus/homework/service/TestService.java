package ru.otus.homework.service;

/**
 * Сервис тестирования.
 */
public interface TestService {

    /**
     * Провести тестирование и получить оценку.
     *
     * @return оценка
     */
    int getMark();

}

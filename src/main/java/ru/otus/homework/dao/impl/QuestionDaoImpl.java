package ru.otus.homework.dao.impl;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Repository;
import ru.otus.homework.benchmark.BenchmarkGetQuestionList;
import ru.otus.homework.dao.QuestionDao;
import ru.otus.homework.domain.Question;
import ru.otus.homework.properties.AppProps;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * DAO вопросов к тестированию.
 */
@Repository("questionDao")
public class QuestionDaoImpl implements QuestionDao {

    /**
     * Настройки и (вычисляемые) свойства приложения
     */
    private final AppProps appProps;

    // Constructors ---

    /**
     * @param appProps настройки и (вычисляемые) свойства приложения
     */
    public QuestionDaoImpl(final AppProps appProps) {
        this.appProps = appProps;
    }

    // Public methods ---

    /**
     * Получить список вопросов теста.
     *
     * @return список вопросов теста
     */
    @Override
    @BenchmarkGetQuestionList
    public List<Question> getQuestionList() {

        String localizedCsvPath = this.appProps.getLocalizedCsvFilePath();
        final InputStream testCsvInputStream = ClassLoader.getSystemResourceAsStream(localizedCsvPath);
        final InputStreamReader testCsvInputStreamReader = new InputStreamReader(testCsvInputStream);
        final CSVReader testCsvReader = new CSVReader(testCsvInputStreamReader);

        final CsvToBeanBuilder<Question> csvToBeanBuilder = new CsvToBeanBuilder<>(testCsvReader);
        csvToBeanBuilder.withSeparator(';');
        csvToBeanBuilder.withType(Question.class);

        final List<Question> questionList = csvToBeanBuilder.build().parse();
        return questionList;
    }
}

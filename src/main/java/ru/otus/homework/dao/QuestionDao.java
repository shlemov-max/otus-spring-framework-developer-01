package ru.otus.homework.dao;

import ru.otus.homework.domain.Question;

import java.util.List;

/**
 * DAO вопросов к тестированию.
 */
public interface QuestionDao {

    /**
     * Получить список вопросов теста.
     *
     * @return список вопросов теста
     */
    List<Question> getQuestionList();

}

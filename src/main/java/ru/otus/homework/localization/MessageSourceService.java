package ru.otus.homework.localization;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import ru.otus.homework.properties.AppProps;

/**
 * Обёртка над локализованным источником сообщений.
 */
@Service
public class MessageSourceService {

    /**
     * Настройки и (вычисляемые) свойства приложения
     */
    private final AppProps appProps;

    /**
     * Источник локализованных сообщений.
     */
    private final MessageSource messageSource;

    // Constructors ---

    /**
     * @param appProps текущиe настройки и (вычисляемые) свойства приложения
     */
    public MessageSourceService(final AppProps appProps){

        this.appProps = appProps;

        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("/i18n/bundle");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setBasename("classpath:localization/bundle");
        this.messageSource = messageSource;
    }

    // Public methods ---

    /**
     * @param messageKey ключ сообщения
     * @param params параметры сообщения
     * @return локализованное сообщение
     */
    public String getLocalizedMessage(final String messageKey, final String[] params){
        final String localizedMessage =  this.messageSource.getMessage(messageKey, params, this.appProps.getCurrentLocale());
        return localizedMessage;
    }

    // Getters & setters ---
}

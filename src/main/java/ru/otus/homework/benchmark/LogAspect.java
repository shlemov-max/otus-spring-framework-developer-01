package ru.otus.homework.benchmark;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {

    @Before("target(ru.otus.homework.dao.QuestionDao)")
    public void log(JoinPoint joinPoint) throws Throwable {
        System.out.println("\nEntered to method:" + joinPoint.getSignature().getName());
    }
}

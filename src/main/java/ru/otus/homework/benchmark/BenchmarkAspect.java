package ru.otus.homework.benchmark;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import ru.otus.homework.domain.Question;

import java.util.List;

/**
 * Аспект для изменения времени выполнения методов.
 */
@Aspect
@Component
public class BenchmarkAspect {

    /**
     * Измерить время выполнения методa QuestionDao.getQuestionList()
     */
    // @Around("@annotation(ru.otus.homework.benchmark.BenchmarkGetQuestionList)") // По аннотации
    @Around("target(ru.otus.homework.dao.QuestionDao)") // По интерфейсу
    public List<Question> getQuestionList (final ProceedingJoinPoint joinPoint) throws Throwable{

        final long begin = System.currentTimeMillis();
        final List<Question> questionList = (List<Question>) joinPoint.proceed();
        final long end = System.currentTimeMillis();

        System.out.println(String.format("\nTime to getQuestionList() is: %d milliseconds.", (end-begin)));
        return questionList;
    }
}

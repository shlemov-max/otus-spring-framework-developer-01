package ru.otus.homework;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import ru.otus.homework.service.ProcessService;


@ComponentScan
@PropertySource("classpath:application.properties")
@EnableAspectJAutoProxy
public class Main {

    public static void main(String[] args) {
        final AnnotationConfigApplicationContext annConfAppCtx = new AnnotationConfigApplicationContext(Main.class);
        final ProcessService processService = annConfAppCtx.getBean(ProcessService.class);
        processService.start();
    }
}
